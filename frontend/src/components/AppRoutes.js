import { connect } from "react-redux";
import { Navigate } from "react-router-dom";
import Logout from "../Pages/Logout/Logout";

function AppRoutes({user}) {
  

  return <>{user ? <Logout /> : <Navigate to="/" />}</>;
}

const mapStateToProps = (state) => ({
  user: state.auth.user
})

export default connect(mapStateToProps)(AppRoutes);
