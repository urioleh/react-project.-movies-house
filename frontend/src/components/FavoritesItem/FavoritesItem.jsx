import {Portal, Menu, MenuItem, MenuGroup, MenuList, MenuButton, Tooltip, useDisclosure, Box, Button, IconButton,  Text, AlertDialog, AlertDialogOverlay, AlertDialogBody, AlertDialogContent, AlertDialogCloseButton, AlertDialogFooter, AlertDialogHeader, useColorModeValue } from '@chakra-ui/react'
import { useMediaQuery } from "@chakra-ui/react"
import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalBody,
    ModalFooter,
    ModalCloseButton,
  } from '@chakra-ui/react'
import { CheckIcon, ChevronDownIcon } from '@chakra-ui/icons'
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import styles from './FavoritesItem.module.scss'
import { toast } from 'react-toastify';
import {BlockPicker} from 'react-color'
import Tippy from '@tippyjs/react'
import { useRef } from 'react';
import InputEmoji from 'react-input-emoji'

export default function FavoritesItem({ item, i, handleDelete, data }) {

    const [isMobile] = useMediaQuery("(max-width: 560px)") 

    const navigate = useNavigate()
    const [change, setChange] = useState("");

    console.log(item)

    // GUARDAR EL LOCALSTORAGE CUANDO ELIMINAMOS UN FAVORITO
    useEffect(() => {
        localStorage.setItem('fav', JSON.stringify(data))
    }, [data]);
       

    // CAMBIAR PROPIEDADES DEL ARRAY DE OBJETOS GUARDADO EN LOCAL STORAGE (Y PONER TOAST)
        const handleChange = (id, quote) => {
            const newData = data.map(a => {
                if (a.id === id) {
                    a.type = quote
                    console.log(a)
                    return a
                } else {
                    return a
                }
                
            })
            console.log(newData)
            localStorage.setItem("fav", JSON.stringify(newData))
            toast.success('Mooving to ' + quote, {
                position: "top-center",
                autoClose: 1500,
                icon: "🏃",
                hideProgressBar: false,                    /* NOTIFICACIÓN */
                closeOnClick: false,
                pauseOnHover: false,
                draggable: false,
                progress: undefined,
                });
            setTimeout(() => {
                window.location.reload()    /* RECARGAMOS PARA QUE SE APLIQUEN AL INSTANTE LOS CAMBIOS DEL LOCAL STORAGE YA QUE LA PAGINA PINTA LO QUE HAY EN EL LOCALSTORAGE */
            }, 1500);

            
        }
        // CAMBIAR COLOR DEL OBJETO DEL LOCAL STORAGE (IGUAL QUE ARRIBA PERO EL COLOR)
        const handleColor = (id, color) => {
            const newData = data.map(a => {
                if (a.id === id) {
                    a.color = color             
                    console.log(a)
                    return a
                } else {
                    return a
                }
                
            })
            console.log(newData)
            localStorage.setItem("fav", JSON.stringify(newData))
            window.location.reload() 
        }
        
        const { isOpen, onOpen, onClose } = useDisclosure()
        const { isOpen: isOpen2, onOpen: onOpen2, onClose: onClose2 } = useDisclosure()
        const cancelRef = useRef()
        const finalRef = useRef()
        
                const handleOnContextMenu = (ev) => {
                    ev.preventDefault()
                    onOpen()
                }

    return(
        <>
        <Box bg={useColorModeValue("gray.100", "gray.700")} key={item.id} className={styles.container} style={{borderColor: item.color }}>
            <div className={styles.firstLine}>
                <div style={{width: 100}}></div>
                    <div className={styles.aspects}>
                        <div className={styles.watchWithDiv}>
                        <Button colorScheme='blue' size='sm' variant="outline" title="change" onClick={ ()=> handleChange(item.id, "Need N0W 😍")}>Need NOW </Button>
                        <Button colorScheme='blue' size='sm' variant="outline" title="change" onClick={ ()=> handleChange(item.id, "Some day...😗 ")}>Some day... </Button>
                        <Button colorScheme='blue' size='sm' variant="outline" title="change" onClick={ ()=> handleChange(item.id, "Pendent")}>Pendent </Button>
                        </div>
                            <Text className={styles.or}>Or</Text>
                            <form className={styles.form}  onSubmit={ (ev) => ev.preventDefault()}>
                            <InputEmoji
                            maxLength="15"
                                
                                onChange={setChange}
                                cleanOnEnter
                                borderRadius="8px"
                                onEnter={ ()=> handleChange(item.id,(change) )}
                                placeholder="Custom your message..."
                            />                   
                                <IconButton className={styles.iconButton} colorScheme='blue' h='1.65rem' type="submit" size='sm' title="change" onClick={ ()=> handleChange(item.id,(change) )} icon={<CheckIcon />} />
                            </form>
                    </div>  

                    <Menu  className={styles.menu} >
                        <MenuButton  display={isMobile ? "block" : "none" } as={Button} colorScheme='blue' variant="outline" rightIcon={<ChevronDownIcon />}>
                            Type
                            
                        </MenuButton>
                        <Portal>
                        <MenuList>
                            <MenuGroup>                               
                                    <MenuItem  onClick={ ()=> handleChange(item.id, "Need N0W 😍")}>
                                        Need NOW 
                                    </MenuItem>                               
                                    <MenuItem onClick={ ()=> handleChange(item.id, "Some day...😗 ")}>
                                    Some day...
                                    </MenuItem>
                                    <MenuItem onClick={ ()=> handleChange(item.id, "Pendent")}>
                                    Pendent
                                    </MenuItem>
                                    <MenuItem isDisabled={true} style={{"opacity" : 1, "width" : "100", "cursor": "default"}}>
                                    <form className={styles.formMobile} onSubmit={ (ev) => ev.preventDefault()}>
                            <InputEmoji
                                maxLength="12" 
                                onChange={setChange}
                                cleanOnEnter
                                borderRadius="8px"
                                onEnter={ ()=> handleChange(item.id,(change) )}
                                placeholder="Custom message..."
                            />                   
                                <IconButton className={styles.iconButton} colorScheme='blue' h='1.65rem' type="submit" size='sm' title="change" onClick={ ()=> handleChange(item.id,(change) )} icon={<CheckIcon />} />
                            </form>
                                    </MenuItem>
                            </MenuGroup>
                        </MenuList>
                        </Portal>
                    </Menu>
   
            </div>
            <Tooltip placement='top-start' label='Right click: image & trailer' fontSize='md'>
            <Text as="h4"  size="lg" onContextMenu={handleOnContextMenu} onClick={() => navigate(`/movie/${item.id}`)} className={styles.name} >{ item.name }</Text>
            </Tooltip>
            <Modal className={styles.modal} finalFocusRef={finalRef} isOpen={isOpen} onClose={onClose}>
                <ModalOverlay />
                <ModalContent>
                <ModalCloseButton />
                <ModalBody className={styles.modal} >
                    <img src={`https://image.tmdb.org/t/p/w300${item.image}`}alt="img" />
                </ModalBody>
                <ModalFooter display="flex" justifyContent="space-around" className={styles.modalFooter} >
            <Button as="a" target="_blank" rel="noreferrer"href={`https://www.youtube.com/results?search_query=${item.name}+trailer`}>
                Search Trailer
            </Button>
            <Button colorScheme='blue' mr={3} onClick={onClose}>
              Close
            </Button>
          </ModalFooter>
                </ModalContent>
            </Modal>
            
            <div className="itemState">
                <Box 
                w={isMobile ? 180 : 200 }
                bg={item.color}
                
                className={styles.type} style={{borderColor: item.color}}>{item.type}
                </Box>
                </div>
                <div className={styles.bottomLine}>
                    <div>
                {/* TIPPY PARA TENER COSAS ESCONDIDAS QUE SALEN AL HACER HOVER DEL BUTTON DE DENTRO */}
                        <Tippy interactive={true} placement={'top'} content={
                            <BlockPicker    /* VENTANA DE ELEGIR COLOR */
                            color={item.color}
                            onChangeComplete={color => handleColor(item.id, color.hex)}
                            />
                        } >
                        <Button
                        bg={useColorModeValue("gray.600", "gray.300")}
                        color={useColorModeValue("gray.100", "gray.700")}
                        size='sm'
                        _hover={{ bg: 'gray.400' }}>Change color </Button>
                        </Tippy>
                    </div>
                                <Button colorScheme='gray' size='sm' title="delete" onClick={onOpen2} >Delete</Button>
                                <AlertDialog
                                    motionPreset='slideInBottom'
                                    leastDestructiveRef={cancelRef}
                                    onClose={onClose2}
                                    isOpen={isOpen2}
                                    isCentered
                                >
                                    <AlertDialogOverlay />

                                    <AlertDialogContent>
                                    <AlertDialogHeader>Really? 😱</AlertDialogHeader>
                                    <AlertDialogCloseButton />
                                    <AlertDialogBody>
                                        Are you sure you want to discard this movie?
                                    </AlertDialogBody>
                                    <AlertDialogFooter>
                                        <Button ref={cancelRef} onClick={onClose2}>
                                        No
                                        </Button>
                                        <Button colorScheme='red'  ml={3}  onClick={ ()=> handleDelete(item.id)}>
                                        Yes
                                        </Button>
                                    </AlertDialogFooter>
                                    </AlertDialogContent>
                                </AlertDialog>
                </div>
            
        </Box>

        </>
    )
}

