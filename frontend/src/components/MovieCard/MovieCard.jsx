import { Link as ReachLink } from "react-router-dom";
import { Heading, Tag, Link } from '@chakra-ui/react'
import styles from "./MovieCard.module.scss"

export default function MovieCard({ movie }) {

    const imageUrl = movie.poster_path ? "https://image.tmdb.org/t/p/w300" + movie.poster_path : "https://www.genius100visions.com/wp-content/uploads/2017/09/placeholder-vertical.jpg"

    return (
        <li className={styles.item}>
  
            <div >
            <Link  className={styles.link} to={`/movie/${movie.id}`} >
              <Heading as="h4" size="sm" className={styles.title}>{movie.original_title}</Heading>
            </Link>
            <Tag className={styles.tag} size="md" variant='solid' colorScheme='yellow'>
            {movie.vote_average}
            </Tag>
              <Link  as={ReachLink} to={`/movie/${movie.id}`} > <img width={230} height={345} src={imageUrl} alt={movie.original_title} /></Link>
            </div>
         
        </li>
      );
}
