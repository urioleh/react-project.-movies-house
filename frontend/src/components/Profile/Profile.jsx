import Login from "../../Pages/Login/Login";
import { YourProfile } from "../../components";
import { connect } from "react-redux";
import styles from './Profile.module.scss'

function Profile({user}) {
    document.title = "Your Profile";

    return (
        <div className={styles.profile}>
           {user ? <YourProfile/> : <Login/> } 
          
        </div>
    )
   }

   const mapStateToProps = (state) => ({
       user: state.auth.user
   })

   export default connect(mapStateToProps)(Profile)