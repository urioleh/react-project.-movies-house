import { Tag, Text, Badge, Box } from '@chakra-ui/react'
import { Accordion, AccordionItem, AccordionButton, AccordionPanel, AccordionIcon } from '@chakra-ui/react'
import { useContext, useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { get } from "../../utils/httpClient";
import { AddMovie } from "../../components";
import FavoritesContext from "../../contexts/FavoritesContext";
import styles from "./MovieDetails.module.scss";
import ReactStars from 'react-stars'
import { UseStateContext } from '../../contexts/UseStateContext';

export default function MovieDetails({ colorMode }) {
    const { movieId } = useParams(); /* USEPARAMS PARA COGER LA RUTA QUERY */
    const [movie, setMovie] = useState([]);
    const [genres, setGenres] = useState([]);
    const [companies, setCompanies] = useState([]);

    const [nameMovie, setNameMovie] = useState("");
    const [idMovie, setIdMovie] = useState("");
    const [imageMovie, setImageMovie] = useState("");

    const { data, handleAdd } = useContext(FavoritesContext);

    const context = useContext(UseStateContext)
    const { /*genre*/  setGenre } = context

    const navigate = useNavigate()

    // HACER GET EN LA RUTA DE LA API COMPLETANDOLA CON LA ID DE LA PELICULA QUE COGEMOS CON EL USEPARAMS
    useEffect(() => {
        get(`/movie/${movieId}`)
            .then((response) => {
                console.log(response);
                setMovie(response);

                setGenres(response.genres);
                setCompanies(response.production_companies);

                setNameMovie(movie.original_title);
                setIdMovie(movie.id);
                setImageMovie(movie.poster_path);
            })
            .catch((err) => {
                console.error(err);
            });
    }, [movieId, movie.original_title, movie.id, movie.poster_path]);


    if (!movie) {
        return null;
    }
    const calculate = () => {
        const puntuation = movie.vote_average ? movie.vote_average * 90 / 100 * 10 / 100 * 5 : 50;
        if (puntuation >= 0 && puntuation < 0.25) {
            return 0
        } else if (puntuation >= 0.25 && puntuation < 0.75) {
            return 0.5
        } else if (puntuation >= 0.75 && puntuation < 1.25) {
            return 1
        } else if (puntuation >= 1.25 && puntuation < 1.75) {
            return 1.5
        } else if (puntuation >= 1.75 && puntuation < 2.25) {
            return 2
        } else if (puntuation >= 2.25 && puntuation < 2.75) {
            return 2.5
        } else if (puntuation >= 2.75 && puntuation < 3.25) {
            return 3
        } else if (puntuation >= 3.25 && puntuation < 3.75) {
            return 3.5
        } else if (puntuation >= 3.75 && puntuation < 4.25) {
            return 4
        } else if (puntuation >= 4.25 && puntuation < 4.75) {
            return 4.5
        } else if (puntuation >= 4.75 && puntuation < 5) {
            return 5
        }
    }

    document.title =
        "Movie: " +
        movie.title; /* TITLE PAGE (EL TEXTO DE LA PESTAÑITA DE CHROME) */

    const imageUrl = movie.poster_path
        ? "https://image.tmdb.org/t/p/w300" + movie.poster_path
        : "https://www.genius100visions.com/wp-content/uploads/2017/09/placeholder-vertical.jpg";

    function formatMoney(number) {
        return number.toLocaleString('en-US', { style: 'currency', currency: 'USD' });
    }
   
   


    return (
        <div className={styles.details}>
            <Text as="h2" className={styles.name}>{movie.original_title}</Text>
            <div className={styles.grid}>
                <div className={styles.columnLeft}>
                    <img width={230} height={345} src={imageUrl} alt="" />
                    <div className={styles.leftDown}>
                        <AddMovie
                            handleAdd={handleAdd}
                            nameMovie={nameMovie}
                            idMovie={idMovie}
                            data={data}
                            imageMovie={imageMovie}
                        />
                        <ReactStars
                            count={5}
                            size={24}
                            color2={'#ffd700'}
                            value={calculate()}
                            half={true}
                        />
                        <Text className={styles.score}>Score: {movie.vote_average}/10</Text>
                    </div>
                </div>
                <div className={styles.columnRight}>
                    <div className={styles.rightTop}>
                        <ul className={styles.genres}>
                            {genres.map((genre) => {

                                return (
                                    <li key={genre.id}>
                                        
                                            <Badge  onClick={ () => {setGenre(genre.name); navigate(`/genres/${genre.id}`) }  } variant='outline' colorScheme='yellow' >
                                                {genre.name}
                                            </Badge>
                                        
                                    </li>
                                );
                            })}
                        </ul>
                        <div className={styles.companies}>{companies.map(company => {
                            const imageCompany = "https://image.tmdb.org/t/p/w300" + company.logo_path
                                ;
                                if (company.logo_path) {
                                   return (
                                 <img className={styles.companyImage} key={company.id} src={imageCompany} alt={company.name} />
                    
                                 ) 
                                } return <div key={company.id}></div>
                        })}
                        </div>
                    </div>
                    
                    <div className={styles.rightBottom}>
                        <Text>
                            <Tag size="md" variant='subtle' >
                                Release date       
                            </Tag>
                            {movie.release_date}
                        </Text>
                        <Text>
                        <Tag size="md" variant='subtle' >
                                Duration       
                            </Tag>
                            {movie.runtime} min
                            </Text>
                        {movie.revenue && <Text>
                            <Tag size="md" variant='subtle' >
                                Revenue      
                            </Tag> 
                            {formatMoney(movie.revenue)} 
                            </Text>}
                        <div className={styles.sinopsi}>
                            <Accordion allowToggle>
                                <AccordionItem>
                                    <h2>
                                    <AccordionButton _expanded={{ bg: 'yellow.400', color: 'white' }}>
                                        <Box flex='1' textAlign='left'>
                                            Synopsis
                                        </Box>
                                        <AccordionIcon />
                                    </AccordionButton>
                                    </h2>
                                    <AccordionPanel>
                                        {movie.overview}
                                    </AccordionPanel>
                                </AccordionItem>
                            </Accordion>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

