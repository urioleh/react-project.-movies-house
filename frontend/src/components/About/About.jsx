import styles from './About.module.scss'
import myCV from '../../assets/myCV.png'
import { Tooltip } from '@chakra-ui/react'

export default function About() {
    document.title = "About Page";

    return (

        <div className={styles.about}>
            <div  className={styles.container}>
                <h1>Go to my resume</h1>
                <Tooltip hasArrow placement='top' label='Click to go!' fontSize='lg'>
                <img src={myCV} alt="" />
                </Tooltip>
                <h3>Probably i'm waiting you!</h3>
            </div>
        </div>
    )
   }