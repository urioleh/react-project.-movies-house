import { Text } from "@chakra-ui/react";
import { useEffect, useState, useContext } from "react";
import { Link, useSearchParams } from "react-router-dom";
import { get } from "../../utils/httpClient";
import styles from './SearchedMovies.module.scss'
import ReactPaginate from 'react-paginate';
import { UseStateContext } from "../../contexts/UseStateContext"
import { animateScroll as scroll } from "react-scroll";

export default function SearchedMovies() {
  const [movies, setMovies] = useState("");

  const [searchParams] = useSearchParams(); /* PARA RECOGER LOS SEARCHPARAMS QUE HEMOS ESCRITO EN EL INPUT (COMPONENTE Searcher) */
  
  /* FLAG PARA AVISARLE AL INFINITE SCROLL QUE NO HAY MAS PÁGINAS */
  const [totalPages, setTotalPages] = useState('');

  const context = useContext(UseStateContext)
  const { page, setPage } = context

//   NOS DIRIJIMOS A LA RUTA DE LA API QUE CREAMOS CON LOS PARAMS ESCRITOS DESDE EL INPUT
  useEffect(() => {
    get(`/search/movie?&query=${searchParams}&page=${page}`)
      .then((response) => {
        console.log(response);
        setMovies(response.results)    /* PARA QUE NO SE SUSTITUYAN Y FUNCIONE EL INFINITE SCROLL */
        setTotalPages(response.total_pages)
        scroll.scrollToTop();

      })
      .catch((err) => {
        console.error(err);
      });
  }, [searchParams, page]);



  const handlePageClick = (data) => {
   console.log(data.selected)

   let currentPage = data.selected + 1

   setPage(currentPage)
   scroll.scrollToTop();
  };



  if (!totalPages || !movies) {
    return (
      <div className={styles.notFound}>
        <h3>Not Found Results... </h3>
        <p>Try another thing</p>
      </div>
    );
  }

  const title = searchParams.toString().slice(0, -1)
  

  return (

    <div>
      <Text as='h3' size='lg' className={styles.title}> Results for: {title} </Text>

      <ul className={styles.moviesGrid}>
        {movies.map((movie) => {
          const imageUrl = movie.poster_path
            ? "https://image.tmdb.org/t/p/w300" + movie.poster_path
            : "https://www.genius100visions.com/wp-content/uploads/2017/09/placeholder-vertical.jpg";
          return (
            <li className={styles.movieCard} key={movie.id}>
              <Link to={`/movie/${movie.id}`}>
                <h1>{movie.original_title}</h1>
              </Link>
              <Link to={`/movie/${movie.id}`}>
                <img width={230} height={345} src={imageUrl} alt="img" /> {/* WIDTH I HEIGHT DEFINIDOS AHI PARA QUE LA IMAGEN OCUPE ESPACIO INCLUSO ANTES DE CARGARSE*/}
      
              </Link>
              <p>{movie.vote_average}</p>
              <p>{movie.release_date}</p>
            </li>
          );
        })}
      </ul>
        <div className={styles.back}></div>
          <ReactPaginate
          previousLabel="previous"
          nextLabel="next"
          breakLabel="..."
          pageCount={totalPages}
          onPageChange={handlePageClick}
          renderOnZeroPageCount={null}
          containerClassName={styles.paginate}
          pageClassName={styles.page}
          pageLinkClassName={styles.pageA}
          previousClassName={styles.previous}
          previousLinkClassName={styles.pageAPrev}
          nextClassName={styles.next}
          nextLinkClassName={styles.pageANext}
          disabledLinkClassName={styles.disabled}
          prevRel={page <=1 ? null: undefined}
          nextRel={page >= totalPages ? null: undefined}
          
        />
    </div>

  );
}
