import { useNavigate, useSearchParams} from "react-router-dom";
import { Input, IconButton, InputGroup, InputRightElement } from '@chakra-ui/react'
import {
    Popover,
    PopoverTrigger,
    PopoverContent,
  } from '@chakra-ui/react'
import { SearchIcon  } from '@chakra-ui/icons'
import { useMediaQuery } from "@chakra-ui/react"
import { useContext, useRef } from "react";
import { UseStateContext } from "../../contexts/UseStateContext";
import styles from './Searcher.module.scss'

export default function Searcher({ search, setSearch }) {


const [isMobile] = useMediaQuery("(max-width: 560px)") 

const navigate = useNavigate()
const [searchParams, setSearchParams] = useSearchParams() /* PARA ESCRIBIR EN LOS QUERYPARAMS */

const context = useContext(UseStateContext)
const { /* page */ setPage } = context

const initialFocusRef = useRef()


// AL HACER SUBMIT VIAJAMOS A LA RUTA QUE HEMOS CREADO AÑADIENDO LO QUE ESCRIBIMOS EN LOS PARAMS DIRECTAMENTE DESDE EL ONCHANGE DEL INPUT
const handleSubmit = (ev) => {
    ev.preventDefault();

    navigate(`/search/movies/?${searchParams}`)
    console.log(search)
    setSearch('')
    window.location.reload()
}

// FUNCIÓN PARA SACAR EL VALOR DEL INPUT Y METERLO EN STATE Y EN STATEPARAMS(QUERY)
const handleChange = (ev) => {
    const { value } = ev.target
    setSearch(value)
    setSearchParams(value)
    setPage(1)
  }

return (
    <>  
        <form className={styles.searcher} onSubmit={handleSubmit}>
        <InputGroup   maxWidth="300" size='md'>
        <Input border='2px' type="text" onChange={handleChange} placeholder="Search movie" />
        <InputRightElement width='2.5rem'>
        <IconButton  h='1.65rem' type="submit" size='sm' title="change"  icon={<SearchIcon />} />
        </InputRightElement>
        </InputGroup>
        </form>

        
        <Popover className={styles.searcherMobile} initialFocusRef={initialFocusRef}>
        <PopoverTrigger >
        <IconButton
            display={isMobile ? "block" : "none" }
            aria-label='Search database'
            icon={<SearchIcon />}
        />
        </PopoverTrigger>      
            <PopoverContent>
                <form className={styles.searcherMobileForm} onSubmit={handleSubmit}>
                    <InputGroup size='md' >
                    <Input ref={initialFocusRef} border='2px' type="text" onChange={handleChange} placeholder="Search movie" />
                    <InputRightElement width='2.5rem'>
                    <IconButton  h='1.65rem' type="submit" size='sm' title="change"  icon={<SearchIcon />} />
                    </InputRightElement>
                    </InputGroup>
                </form>
            </PopoverContent>
        </Popover>
    </>
) 

}

                            