import {Portal, Avatar, IconButton, Box, Menu, MenuItem, MenuGroup, MenuList, MenuButton, MenuDivider, Tooltip, Button, useColorModeValue } from '@chakra-ui/react'
import { ArrowDownIcon, ChevronDownIcon, StarIcon, QuestionOutlineIcon } from '@chakra-ui/icons'

import { NavLink, useNavigate } from "react-router-dom"
import sun from '../../assets/sun.png'
import moon from '../../assets/moon.png'
import m from '../../assets/m.png'
import styles from './Header.module.scss'
import { Searcher } from "../../components"
import { connect } from "react-redux"
import { useState, useEffect } from 'react'
import { animateScroll as scroll } from "react-scroll";

function Header({ search, setSearch, colorMode, toggleColorMode, user }) {

    const [disabled, setDisabled] = useState(false);

    const profileImage = localStorage.userImage ? localStorage.userImage : "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png";
 

    useEffect(() => {
      if (user) {
        setDisabled(true)
        return
      } else {
        setDisabled(false)
      }
    }, [user]);

    const navigate = useNavigate()

    const scrollToBottom = () => {
        scroll.scrollToBottom();
    };

    return (
        
            <Box as="nav" bg={useColorModeValue("gray.300", "gray.700")} className={styles.nav}>              {/* NAVLINK ES COMO LINK PERO CON isActive PARA SEÑALAR LA RUTA ACTIVA */}

           
                <img className={styles.m} onClick={() => navigate("/")} src={m} alt="logo" />
           
                <div className={styles.mid}>
                    <img className={styles.imgTheme} onClick={toggleColorMode} src={colorMode === "dark" ? sun : moon} alt="" />
                    <Tooltip hasArrow placement='bottom-start' label='Dark/Light mode' fontSize='md'>
                    <QuestionOutlineIcon className={styles.info} w={3} h={3} />
                    </Tooltip>
                    <Searcher search={search} setSearch={setSearch} />
                <Button as={NavLink}  className={`${({ isActive }) => (isActive ? styles.active : "")} ${styles.genres}`} to="/genres">Genres</Button>
                </div>

                <div className={styles.profileSet}>
                    <div onClick={ () => navigate('/profile') } className={styles.imgUser}>
                    {user ? <Avatar name='profile' size="sm" src={profileImage ? profileImage : "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png" } /> : <div></div> }
                    </div>
                    <Menu className={styles.menu} offset={[0,30]} >
                        <MenuButton as={Button} colorScheme='yellow' rightIcon={<ChevronDownIcon />}>
                            Profile
                            <br />
                        </MenuButton>
                        <Portal>
                        <MenuList>
                            <MenuGroup title={user ? `Profile of ${user.username}` : "Profile Menu"}>
                                <NavLink className={({ isActive }) => (isActive ? styles.active : "")} to="/profile">
                                    <MenuItem>{user ? "My Account" : "Login" }</MenuItem>
                                </NavLink>
                                <NavLink className={({ isActive }) => (isActive ? styles.active : "")} to="/favorites">
                                    <MenuItem icon={<StarIcon/>} isDisabled={!disabled}>Favorites </MenuItem>
                                </NavLink>
                            </MenuGroup>
                            <MenuDivider />
                            <MenuGroup color="gray.400" title='Help' >
                                <NavLink className={({ isActive }) => (isActive ? styles.active : "")} to="/about">
                                    <MenuItem command="The Author">About me</MenuItem>
                                </NavLink>
                            </MenuGroup>
                        </MenuList>
                        </Portal>
                    </Menu>
                </div>
            <IconButton
            className={styles.scrollDown}
            onClick={scrollToBottom}
            variant='outline'
            colorScheme='yellow'
            aria-label='Send email'
            size='sm'
            icon={<ArrowDownIcon />}
            />
            </Box>
        
    )
}

const mapStateToProps = (state) => ({
    user: state.auth.user,
})

export default connect(mapStateToProps)(Header);

