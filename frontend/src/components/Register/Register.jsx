import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons";
import { Button, FormLabel, Text, Input, InputGroup, InputRightElement } from "@chakra-ui/react";
import { useState } from "react";
import { connect } from "react-redux";
import { useNavigate } from 'react-router-dom';
import { registerUser } from "../../redux/actions/auth.actions";
import styles from './Register.module.scss'

const INITIAL_STATE = {
    username: '',
    email: '',
    password: '',
}

function Register({dispatch, error}) {


    const [form, setForm] = useState(INITIAL_STATE);

    const change = ({target: {name, value}}) => setForm({ ...form, [name]: value })
    
    const navigate = useNavigate();

    const [show, setShow] = useState(false)
    const handleClick = () => setShow(!show)

    const submitForm = (ev) => {
        ev.preventDefault();

        try {
           
            dispatch(registerUser(form));
           
            navigate('/profile');
        } catch (error) {
            console.log('Catch', error);
        }
        
    }

    return(
        <div className={styles.register}>
        <Text as='h2' size='2xl' className={styles.title} >
        Register
        </Text>
        
        <form className={styles.form} onSubmit={submitForm}>
        <div className={styles.loginItem1}>
        <FormLabel htmlFor='username'>Username</FormLabel>
        <Input required={true} name='username' id="username" variant='flushed' type='username' placeholder='Ex: LadyDuck' onChange={change} value={form.username} ></Input>
        </div>
        <div className={styles.loginItem2}>
        <FormLabel htmlFor='email'>Email</FormLabel>
        <Input required={true} name='email' od="email" variant='flushed' type='email' placeholder='Ex: Thor300@watdafac.com' onChange={change} value={form.email} ></Input>
        </div>
        <div className={styles.loginGroupItem}>
        <FormLabel htmlFor='password'>Password</FormLabel>
        <InputGroup size='md'>
                <Input
                  required={true}
                  name="password"
                  id="password"
                  onChange={change} value={form.password}
                  variant='flushed'
                  pr='4.5rem'
                  type={show ? 'text' : 'password'}
                  placeholder='Ex: sweetKitty1'
                />
                <InputRightElement width='4.5rem'>
                <Button h='1.75rem' size='sm' onClick={handleClick}>
                    {show ? <ViewOffIcon/> : <ViewIcon/>}
                  </Button>
                </InputRightElement>
              </InputGroup>
          </div>
            <Button type="submit" colorScheme='yellow' size='md'>Register</Button>
        </form>

        {error && <div style={{color: 'red'}}>{error}</div>}
        </div>
    )
}


const mapStateToProps = (state) => ({
    error: state.auth.error
})

export default connect(mapStateToProps)(Register)