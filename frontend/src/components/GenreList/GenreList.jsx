import { Link } from '@chakra-ui/react'
import { useContext, useEffect, useState } from "react";
import { Link as ReachLink } from "react-router-dom";
import { get } from "../../utils/httpClient";
import { UseStateContext } from "../../contexts/UseStateContext"
import styles from './GenreList.module.scss'
import Fade from 'react-reveal/Zoom';

export default function GenreList() {

    document.title = "Movies by Genres"; /* TITLE PAGE (EL TEXTO DE LA PESTAÑITA DE CHROME) */
      
    const [ genres, setGenres ] = useState([])
    
    const context = useContext(UseStateContext)
    const {/* genre */ setGenre } = context
    
    // const postParam = (ev) => {
    //     setGenre(ev.target.lastChild.data) /* RESCATAR EL NOMBRE DEL GENERO Y PONERSELO AL STATE DEL CONTEXT PARA PINTARLO EN EL COMPONENTE QUE LE TOCA*/
    // }

    useEffect(() => {
        get("/genre/movie/list")
        .then(response => {
            console.log(response)
            setGenres(response.genres)
         
        })
        .catch(err => {
            console.error(err);
        });
    }, []);
    
    
    

    return (
        <div className={styles.genres}>
            <Fade >
        <ul className={styles.list}>
          {genres.map(genre => <Link as={ReachLink} to={"/genres/" + genre.id} key={genre.id} className={styles.link}> <li className={styles.genre}  onClick={() => {setGenre(genre.name)} }>{genre.name}  <svg viewBox="0 0 70 36">
            <path d="M6.9739 30.8153H63.0244C65.5269 30.8152 75.5358 -3.68471 35.4998 2.81531C-16.1598 11.2025 0.894099 33.9766 26.9922 34.3153C104.062 35.3153 54.5169 -6.68469 23.489 9.31527" />
        </svg></li> </Link>)}
        </ul>
        </Fade>
        </div>
      );
}