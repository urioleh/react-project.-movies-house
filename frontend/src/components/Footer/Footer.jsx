import { Text, Tag, Box, useColorModeValue, Link } from '@chakra-ui/react'
import { EmailIcon, PhoneIcon, ExternalLinkIcon } from '@chakra-ui/icons'
import { useNavigate } from "react-router-dom"
import m from '../../assets/m.png'
import styles from './Footer.module.scss'


export default function Footer() {

    const Navigate = useNavigate()


    return (
        <Box  bg={useColorModeValue("gray.300", "gray.700")} className={styles.footer}>
            <div className={styles.container}>
                <div className={styles.steps}>
                    <Text as="h4" size="sm" ><Tag className={styles.tag}>1</Tag><span onClick={() => Navigate("/profile/register")} className={styles.sign}> Sign up</span></Text>
                    <Text as="h4" size="sm" ><Tag className={styles.tag}>2</Tag> Research on lists</Text>
                    <Text as="h4" size="sm" ><Tag className={styles.tag}>3</Tag> Add to Favorites</Text>
                    <Text as="h4" size="sm" ><Tag className={styles.tag}>4</Tag> Custom your <Text as="span" size="sm" onClick={() => Navigate("/favorites")} className={styles.favorites} >Favorites </Text>and enjoy </Text>
                </div>

                <div className={styles.center}>
                    <img className={styles.m} onClick={() => Navigate("/")} src={m} alt="logo" />
                    <Text className={styles.name} onClick={() => Navigate("/about")}> Oriol Arias Perez</Text>
                </div>

                <div className={styles.right}>
                    <div className={styles.dates}>
                        <Link className={styles.email} href="mailto:urioleh@gmail.com?Subject=Buenas,%20queremos%20trabajar%20contigo%20por..."> <EmailIcon className={styles.emailIcon} w={6} h={4} /> Mail me</Link>
                        <Link className={styles.email} href={`tel:${+34674411302}`}> <PhoneIcon className={styles.emailIcon} w={6} h={4} /> Call me</Link>
                        <Link className={styles.email} target="_blank" href='https://gitlab.com/users/urioleh/projects'> <ExternalLinkIcon className={styles.emailIcon} w={6} h={4} /> My GitLab </Link>
                    </div>
                    
                    <Text className={styles.small}>metro PNG diseñado por Nabile de  <a href="https://es.pngtree.com"> Pngtree.com</a></Text>
                </div>
            </div>
        </Box>
    )
   }