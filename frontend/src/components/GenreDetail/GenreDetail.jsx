import { Text, Link } from '@chakra-ui/react'
import { useContext, useEffect, useState } from "react";
import { Link as ReachLink, useParams } from "react-router-dom";
import { get } from "../../utils/httpClient";
import InfiniteScroll from 'react-infinite-scroll-component'    /* DEPENDENCIA PARA INFINITE SCROLL */ 
import { Spinner } from "../../components";
import { UseStateContext } from "../../contexts/UseStateContext"
import styles from './GenreDetail.module.scss'
import Fade from 'react-reveal/Fade';

export default function GenreDetail({genrename}) {

    const context = useContext(UseStateContext)
    const { genre /*, setGenre*/ } = context
    
    const [ movies, setMovies] = useState([])
    const [ page, setPage] = useState(1)            /* PAGINADO DEL INFINITE SCROLL */
    const [ hasMore, setHasMore] = useState(true)   /* FLAG PARA AVISARLE AL INFINITE SCROLL QUE NO HAY MAS PÁGINAS */

  
    const { genreId } = useParams()

    const urlString = "/discover/movie?api_key=***&language=en-US&sort_by=vote_count.desc&page=1&with_genres=" + genreId;

    useEffect(() => {
        localStorage.setItem("genre", String(genre)) /* GUARDAR EN LOCAL STORAGE EL NOMBRE DEL GENERO PARA QUE NO SE BORRE AL ACUALIZAR PÁGINA YA QUEE VIENE DE UN USESTATE */
    }, [genre]);

    useEffect(() => {
        
        get(urlString + "&page=" + page)
        .then(response => {
            console.log(response)
            setMovies(prevMovies => prevMovies.concat(response.results))    /* PARA QUE NO SE SUSTITUYAN Y FUNCIONE EL INFINITE SCROLL */
            setHasMore(response.page < response.total_pages)
        })
        .catch(err => {
            console.error(err);
        });
    }, [urlString, page]);

    if (!movies) {
        return null;
    }

    
console.log(genrename)

    return (
        <div className={styles.genreDetail}>
        <InfiniteScroll 
        dataLength={movies.length} 
        hasMore={hasMore} 
        next={() => setPage(prevPage => prevPage + 1)}
        loader={<Spinner/>} 
        >
            <Text className={styles.genre} as="h2" size="xl" >{genre}</Text>
            <ul className={styles.moviesGrid}>
                {movies.map(movie => {
                    const imageUrl = movie.poster_path ? "https://image.tmdb.org/t/p/w300" + movie.poster_path : "https://www.genius100visions.com/wp-content/uploads/2017/09/placeholder-vertical.jpg"
                    return (
                        <li className={styles.movieCard} key={movie.id}>
                            <Fade>
                            <Link as={ReachLink} to={`/movie/${movie.id}`} ><Text as="h3" size="md" className={styles.title}>{movie.original_title}</Text></Link>
                            <Link as={ReachLink} to={`/movie/${movie.id}`} > <img className={styles.movieImage}  width={230} height={345} src={imageUrl} alt={movie.original_title} /></Link>
                            <Text>{movie.vote_average}</Text>
                            <Text>{movie.release_date}</Text>
                            </Fade>
                        </li>
                    )
                })}
            </ul>
        </InfiniteScroll>
        </div>
      );
}
