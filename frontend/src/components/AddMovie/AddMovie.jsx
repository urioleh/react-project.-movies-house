import { Button, Link, Box, useColorModeValue } from "@chakra-ui/react";
import { StarIcon, LockIcon } from '@chakra-ui/icons'
import { Link as ReachLink } from "react-router-dom";
import Tippy from "@tippyjs/react";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import styles from "./AddMovie.module.scss"
import { connect } from "react-redux";

function AddMovie({ handleAdd, nameMovie, idMovie, imageMovie,  data, user }) {

  const [disabled, setDisabled] = useState(false);
 

  useEffect(() => {
    if (user) {
      setDisabled(true)
      return
    } else {
      setDisabled(false)
    }
  }, [user]);

  // VARIABLEPARA AÑADIR A FAVORITOS POR FORM
  const handleSubmit = (ev) => {
    ev.preventDefault();

    const newMovie = {
      name: nameMovie,
      id: idMovie,
      image: imageMovie,
      type: "Pendent",
      color: "#ccc",
    };

    //  SI NO HAY NINGUN ELEMENTO DENTRO DEL ARRAY DE OBJETOS QUE COINCIDA SE AÑADE
    if (!data) {
      handleAdd(newMovie);

      toast.success("Moovie added to Favs!", {
        position: "top-right",
        autoClose: 5000,
        icon: "💚",
        hideProgressBar: false /* NOTIFICACIÓN */,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } else {
      if (data.some((i) => i.name.includes(newMovie.name)) === false) {
        handleAdd(newMovie);

        toast.success("Moovie added to Favs!", {
          position: "top-right",
          autoClose: 5000,
          icon: "💚",
          hideProgressBar: false /* NOTIFICACIÓN */,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      } else {
        toast.warn("Movie added previusly", {
          position: "top-center",
          autoClose: 5000,
          icon: "😐",
          hideProgressBar: false /* NOTIFICACIÓN */,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
      }
    }
  };

  // AÑADIR AL LOCALSTORAGE LO QUE HAY EN EL USESTATE DATA
  useEffect(() => {
    localStorage.setItem("fav", JSON.stringify(data));
    console.log(localStorage.fav);
  }, [data]);

  return (
    <>
      <div>
        <Tippy
          interactive={true}
          disabled={disabled}
          placement={"right"}
          content={
            <Box className={styles.alert} bg={useColorModeValue("gray.100", "gray.700")}>
            <p>You must have an account! <LockIcon /> </p>
            <Link color={useColorModeValue("#e9bc27","#f1df79")} as={ReachLink} to='/profile'>Click to become member!</Link>
            </Box> 
          }
        >
          <form className={styles.form} onSubmit={handleSubmit}>
            <Button  leftIcon={<StarIcon />} type="submit" colorScheme='yellow' size="sm" disabled={!disabled} style={disabled === false ? {cursor: "not-allowed"} : {cursor: "pointer"} } title="Agregar">
              ADD TO FAVORITES
            </Button>
          </form>
        </Tippy>
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  user: state.auth.user
})

export default connect(mapStateToProps)(AddMovie)