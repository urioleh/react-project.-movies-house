import React, { useEffect, useState } from "react";
import { get } from "../../utils/httpClient";
import { MovieCard } from "../../components";
import InfiniteScroll from "react-infinite-scroll-component"; /* DEPENDENCIA PARA INFINITE SCROLL */
import styles from "./Main.module.scss";
import {Box, Text, Menu, MenuButton, MenuList, MenuOptionGroup, MenuItemOption, MenuDivider, Button } from '@chakra-ui/react'
import { ChevronDownIcon } from '@chakra-ui/icons'
import m from '../../assets/m.png'


// import Swiper core and required modules
import { Scrollbar } from 'swiper';

import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';

export default function Main() {
  const [bestMovies, setBestMovies] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [page, setPage] = useState(1); /* PAGINADO DEL INFINITE SCROLL */
  const [hasMore, setHasMore] = useState(true); /* FLAG PARA AVISARLE AL INFINITE SCROLL QUE NO HAY MAS PÁGINAS */
  const [sort] = useState(localStorage.sort ? localStorage.sort : "vote_count.desc" ) 

  const [menuGroup2, setMenuGroup2] = useState('');
  const [menuGroup1, setMenuGroup1] = useState('');

  useEffect(() => {
    get(`/discover/movie?sort_by=${sort}&page= + ${page}`)
      .then((response) => {
        setBestMovies((prevMovies) =>
          prevMovies.concat(response.results)
        ); /* PARA QUE NO SE SUSTITUYAN Y FUNCIONE EL INFINITE SCROLL */

        setHasMore(response.page < response.total_pages);
        setIsLoading(false)
      })
      .catch((err) => {
        console.error(err);
      });
  }, [page, sort]);


  // SELECT PARA CAMBIAR LA RUTA DEL GET Y FILTRAR POR PARAMETROS
  // GUARDAMOS EN LOCAL STORAGE Y RECARGAMOS PÁGINA PARA QUE SE VEA EN PANTALLA LO DEL LOCAL STORAGE
  const changeSort = () => {
    localStorage.setItem("sort", "vote_count.desc")
    const select1 = menuGroup1
    console.log(select1)
    const select2= menuGroup2
    console.log(select2)

    if (select1 === "puntuation" && select2 === "ascending") {
      localStorage.setItem("sort", "vote_count.asc")
      window.location.reload()
    } else if (select1 === "puntuation" && select2 === "descending") {
      localStorage.setItem("sort", "vote_count.desc")
      window.location.reload()
    } else if (select1 === "popularity" && select2 === "ascending") {
      localStorage.setItem("sort", "popularity.asc")
      window.location.reload()
    } else if (select1 === "popularity" && select2 === "descending") {
      localStorage.setItem("sort", "popularity.desc")
      window.location.reload()
    } else if (select1 === "revenue" && select2 === "ascending") {
      localStorage.setItem("sort", "revenue.asc")
      window.location.reload()
    } else if (select1 === "revenue" && select2 === "descending") {
      localStorage.setItem("sort", "revenue.desc")
      window.location.reload()
    }
  };

  const sorted = () => {
    if (localStorage.sort === "vote_count.asc") {
      return "Puntuation Ascending"
    } else if (localStorage.sort === "vote_count.desc") {
      return "Puntuation Descending"
    } else if (localStorage.sort === "popularity.asc") {
      return "Popularity Ascending"
    } else if (localStorage.sort === "popularity.desc") {
      return "Popularity Descending"
    } else if (localStorage.sort === "revenue.asc") {
      return "Revenue Ascending"
    } else if (localStorage.sort === "revenue.desc") {
      return "Revenue Descending"
    }
  }

  
  if (isLoading) {
    return (
      <div className={styles.loading}></div>
    )
  }

  console.log(bestMovies);
  return (
    <div className={styles.main}>
      <InfiniteScroll
        initialScrollY
        dataLength={bestMovies.length}
        hasMore={hasMore}
      >

            <div className={styles.containerAll}>
              <div className={styles.containerTitle} >
                  <img className={styles.m} src={m} alt="logo" />
                  <Text as="h2" size="xl" className={styles.ovies}> OVIE  </Text>
                  <Text as="h2" size="xl" className={styles.list}> LIST </Text>
                  <Box className={styles.box} ></Box>
              </div>
              
                    <div className={styles.sortDiv}>
                      <Menu className={styles.menu} z-index="99" closeOnSelect={false}>
                        <MenuButton as={Button} rightIcon={<ChevronDownIcon />} >
              Sort by...
                        </MenuButton>
                        <MenuList className={styles.menuList} minWidth='240px'>
              <MenuOptionGroup  onChange={(ev) => setMenuGroup2(ev)} title='Order' type='radio'>
                <MenuItemOption value='ascending'>Ascending</MenuItemOption>
                <MenuItemOption value='descending'>Descending</MenuItemOption>
              </MenuOptionGroup>
              <MenuDivider />
              <MenuOptionGroup title='Type' onChange={(ev) => setMenuGroup1(ev)} type='radio'>
                <MenuItemOption value="puntuation">Puntuation</MenuItemOption>
                <MenuItemOption value="popularity">Popularity</MenuItemOption>
                <MenuItemOption value="revenue">Revenue</MenuItemOption>
              </MenuOptionGroup>
                        </MenuList>
                      </Menu>
                      <Button className={styles.button} colorScheme='yellow' onClick={changeSort}>Sort</Button>
                    </div>
              
                    <img className={styles.imgFlecha} src="https://userscontent2.emaze.com/images/aa8021ac-1669-48f7-8a50-0d16fd945ff1/9b851dc68f8145a9e00d2fc62a838314.png" alt="flecha derecha" />
              
                      <Text as="h4" size="sm" className={styles.sortedBy}>Sorted by {sorted()}</Text>
                      
                      <Swiper className={styles.swiper}
                       modules={[ Scrollbar ]}
                       breakpoints={{
                        // when window width is >= 640px
                        0: {
                          width: 360,
                          slidesPerView: 1,
                          spaceBetween:0
                        },
                        1000: {
                          width: 1000,
                          slidesPerView: 3,
                          spaceBetween: 20
                        },
                      }}
                       onReachEnd={() => {
                        return !isLoading && setPage((prevPage) => prevPage + 1)
                        }}
                       scrollbar={{ draggable: true }}
                       onSwiper={(swiper) => console.log(swiper)}>
                        {bestMovies.map((movie) => (
                         <SwiperSlide key={movie.id + Math.random()}> <MovieCard key={movie.id + Math.random()} movie={movie} /></SwiperSlide>
                        ))}
                      </Swiper>
            </div>
      </InfiniteScroll>


    </div>
  );
}
