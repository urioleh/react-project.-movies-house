import { Table, TableCaption, Td, Tr, Tbody, Text, Button, Avatar, Modal, ModalOverlay, ModalContent, ModalHeader, ModalCloseButton, ModalBody, ModalFooter } from "@chakra-ui/react";
import { useDisclosure } from '@chakra-ui/react'
import { connect } from "react-redux";
import Logout from "../../Pages/Logout/Logout";
import styles from './YourProfile.module.scss'

function YourProfile({ user }) {

    const img1 = 'https://i.kym-cdn.com/photos/images/newsfeed/001/561/356/734.jpg'
    const img2 = 'https://http2.mlstatic.com/D_NQ_NP_771053-MLM45306143863_032021-O.jpg'
    const img3 = 'https://www.generacionx.es/Imagenes/Articulos/8425611392603.jpg'
    const img4 = 'https://cl.buscafs.com/www.tomatazos.com/public/uploads/images/296355/296355_600x315.jpg'
    const img5 = 'https://pbs.twimg.com/media/Cmi69mNW8AEJDC7.jpg'
    const img6 = 'https://pbs.twimg.com/profile_images/1281573151747592193/LMHYUNzF_400x400.jpg'
    const img7 = 'https://lumiere-a.akamaihd.net/v1/images/stitch_16x9_9ac13651.png'
    const img8 = 'https://i.pinimg.com/originals/53/25/9a/53259a1f4e675127c61097b4f8726c66.jpg'
    const img9 = 'https://as01.epimg.net/epik/imagenes/2018/11/16/portada/1542384053_864693_1542384302_noticia_normal.jpg'
    
    const imageList = [img1, img2, img3, img4, img5, img6, img7, img8, img9]
    const { isOpen, onOpen, onClose } = useDisclosure()
  
    const image = localStorage.userImage ? localStorage.userImage : "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png";
     
    console.log(image)

    return (
        <>  
            <Text className={styles.title} as='h2' size='xl'>Profile of {user.username} </Text>
            <Logout></Logout>
            <div className={styles.allInfo}>
                <div className={styles.chooseImage}>
                    <Avatar className={styles.avatar} size="xl" name='Profile' src={image ? image : "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png" } />
                    <Button className={styles.button}
                    onClick={() => onOpen()}
                    m={4}
                    >Choose Image</Button>
                </div>
                <div className={styles.info}>
                <Table variant='simple' colorScheme='gray'>
                    <TableCaption>Your personal data is confidential</TableCaption>
                    <Tbody>
                        <Tr>
                        <Td>Nombre:</Td>
                        <Td>{user.username }</Td>
                        </Tr>
                        <Tr>
                        <Td>Email:</Td>
                        <Td>{user.email }</Td>
                        </Tr>
                    </Tbody>
                </Table>
                </div>
            </div>
            <Modal onClose={onClose} size="xl" isOpen={isOpen}>
                <ModalOverlay />
                <ModalContent>
                <ModalHeader>Modal Title</ModalHeader>
                <ModalCloseButton />
                <ModalBody className={styles.containerImg} >
                {imageList.map(image => 
                    <img key={Math.random()}
                    onClick={(ev) =>{
                        localStorage.setItem("userImage", ev.target.src)
                        onClose()
                    }}
                        src={image} alt="avatar" >
                    </img>)}
                </ModalBody>
                <ModalFooter>
                    <Button onClick={onClose}>Close</Button>
                </ModalFooter>
                </ModalContent>
            </Modal>
        </>
    ) 
    
    }


    const mapStateToProps = (state) => ({
        user: state.auth.user
    })
 
    export default connect(mapStateToProps)(YourProfile)
