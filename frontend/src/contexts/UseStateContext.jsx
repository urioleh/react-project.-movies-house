import React, {useState} from 'react';
import { createContext } from 'react'

export const UseStateContext = createContext({})

const UseStateProvider = ({ children }) => {
    const [genre, setGenre] = useState(localStorage.getItem('genre'));
    const [ page, setPage] = useState(1)   

    
    return  (
        <UseStateContext.Provider value={{ genre, setGenre, page, setPage }} >
            { children }
        </UseStateContext.Provider>
    )
}



export default UseStateProvider