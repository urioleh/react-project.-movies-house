import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import App from "./App";
import FavoritesState from "./contexts/FavoritesState";
// Redux
import { Provider } from "react-redux";
import store from "./redux/store";
// 1. import `ChakraProvider` component
import { ChakraProvider } from "@chakra-ui/react";
import { ColorModeScript } from "@chakra-ui/react";
import theme from "./utils/theme";

ReactDOM.render(
    <ChakraProvider>
    <Provider store={store}> 
      <FavoritesState>
        <ColorModeScript initialColorMode={theme.config.initialColorMode} />
        <App />
      </FavoritesState>
    </Provider>
    </ChakraProvider>,
  document.getElementById("root")
);
