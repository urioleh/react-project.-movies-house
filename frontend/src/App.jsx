import {
  Home,
  About,
  Profile,
  Header,
  Footer,
  MovieDetails,
  GenreList,
  GenreDetail,
  SearchedMovies,
  Favorites,
  Register,
} from "./components";

import {
  BrowserRouter as Router,
  Navigate,
  Route,
  Routes,
} from "react-router-dom";
import PrivateRoute from "./utils/PrivateRoute";
import "./App.scss";
import { useEffect, useState } from "react";
import UseStateProvider from "./contexts/UseStateContext";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AppRoutes from "./components/AppRoutes";
import { useColorMode } from "@chakra-ui/react";
import { checkUserSession } from "./redux/actions/auth.actions";
import { connect } from "react-redux";

// import useCounter from "./hooks/exampleHook";

function App({ dispatch, user }) {
  useEffect(() => {
    dispatch(checkUserSession());
  }, []);

  const [search, setSearch] = useState("");

  // PARA EL CAMBIO DE DARK/LIGHT
  const { colorMode, toggleColorMode } = useColorMode();

  // const {counter, sumar, restar} = useCounter()

  return (
    <Router>
      <ToastContainer />
      <UseStateProvider>
        <Header
          search={search}
          setSearch={setSearch}
          colorMode={colorMode}
          toggleColorMode={toggleColorMode}
        />
        <Routes>
          <Route path="*" element={<Navigate replace to="/" />} />
          <Route path="/">
            <Route index element={<Home />} />
            <Route
              path="movie/:movieId"
              element={<MovieDetails colorMode={colorMode} />}
            />
            <Route path="/about">
              <Route index element={<About />} />
            </Route>
            <Route path="/favorites">
              <Route
                index
                element={<PrivateRoute component={<Favorites />} />}
              />
            </Route>
            <Route path="/profile">
              <Route index element={<Profile />} />
              <Route
                path="register"
                element={!user ? <Register /> : <Navigate replace to="/" />}
              ></Route>
            </Route>
            <Route path="/genres">
              <Route index element={<GenreList />} />
              <Route path=":genreId" element={<GenreDetail />}></Route>
            </Route>
            <Route
              path="/search/movies"
              element={<SearchedMovies search={search} setSearch={setSearch} />}
            />
            <Route path="/Logout" element={<AppRoutes />}></Route>
          </Route>
        </Routes>
        <Footer />
      </UseStateProvider>
    </Router>
  );
}

const mapStateToProps = (state) => ({
  user: state.auth.user,
});

export default connect(mapStateToProps)(App);
