 
import React, { useState } from 'react';
import { Link as ReachLink } from 'react-router-dom';
import styles from './Login.module.scss';
import { Button, FormLabel, Input, InputGroup, InputRightElement, Text, Link } from '@chakra-ui/react'
import { ViewOffIcon, ViewIcon } from '@chakra-ui/icons' 
import { loginUser } from '../../redux/actions/auth.actions'
// import { useAuthDispatch, useAuthState } from '../../contexts/LoginContext';
import { connect } from 'react-redux';

const INITIAL_STATE = {
  email: '',
  password: '',
}

function Login({ dispatch, error }) {
  
  const [form, setForm] = useState(INITIAL_STATE);
  // const dispatch = useAuthDispatch()
  


  const [show, setShow] = useState(false)
  const handleClick = () => setShow(!show)

  const inputChange = ({target: {name, value}}) => setForm({ ...form, [name]: value })

  const submitForm = (ev) => {
      ev.preventDefault();
      dispatch(loginUser(form, false));
  };

  return (
    <>
    <div className={styles.container}>
      <div className={styles.formContainer}>
        <Text as='h2' size='xl' className={styles.title}>Login</Text>
 
        <form  className={styles.form} onSubmit={submitForm}>
          <div className={styles.loginForm}>
            <div className={styles.loginFormItem}>
            <FormLabel htmlFor='email'>Email</FormLabel>
              <Input variant='flushed' type='email' placeholder='Ex: Thor300@watdafac.com' id="email" name="email" onChange={inputChange} value={form.email} />
            </div>
            <div className={styles.loginFormItem}>
            <FormLabel htmlFor='password'>Password</FormLabel>
              <InputGroup size='md'>
                <Input
                  name="password"
                  id="password"
                  onChange={inputChange} value={form.password || ""}
                  variant='flushed'
                  pr='4.5rem'
                  type={show ? 'text' : 'password'}
                  placeholder='Ex: sweetKitty1'
                />
                <InputRightElement width='4.5rem'>
                  <Button h='1.75rem' size='sm' onClick={handleClick}>
                    {show ? <ViewOffIcon/> : <ViewIcon/>}
                  </Button>
                </InputRightElement>
              </InputGroup>
            </div>
          </div>
          <Button type="submit" colorScheme='yellow' size='md'>
            Login
          </Button>
      {error && <h4 style={{color: 'red', display: 'block', margin: "0 auto"}}>{error}</h4>}
        </form>
        
      </div>
    </div>
    <div className={styles.register}>
      <Text>You don't have an account?</Text>
      <Link as={ReachLink} to='/profile/register'> Click to become a member!</Link>
    </div>

    </>
  );
}
 

const mapStateToProps = (state) => ({
  error: state.auth.error
})

export default connect(mapStateToProps)(Login)