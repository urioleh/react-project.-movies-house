import { Button } from "@chakra-ui/react";
import React from "react";
import { useNavigate } from "react-router-dom";
import styles from "./Logout.module.scss";
import { connect } from 'react-redux';
import { logoutUser } from '../../redux/actions/auth.actions'

function Logout({ dispatch, error }) {
  let navigate = useNavigate();
  

  const handleLogout = (ev) => {
    ev.preventDefault();
      dispatch(logoutUser());
    navigate("/"); //navega de nuevo al login sin usuario
  };
  return (
    <div>
      <div className={styles.LogoutPage}>
        <Button className={styles.button} colorScheme='red' variant='outline' size="sm" onClick={handleLogout}>
          Logout
        </Button>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  error: state.auth.error
})

export default connect(mapStateToProps)(Logout)