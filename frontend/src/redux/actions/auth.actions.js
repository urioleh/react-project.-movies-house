export const AUTH_REGISTER_OK = 'AUTH_REGISTER_OK';
export const AUTH_REGISTER = 'AUTH_REGISTER';
export const AUTH_REGISTER_ERROR = 'AUTH_REGISTER_ERROR';

export const AUTH_LOGIN_OK = 'AUTH_LOGIN_OK';
export const AUTH_LOGIN = 'AUTH_LOGIN';
export const AUTH_LOGIN_ERROR = 'AUTH_LOGIN_ERROR';

export const CHECK_SESSION = "CHECK_SESSION";
export const CHECK_SESSION_OK = "CHECK_SESSION_OK";
export const CHECK_SESSION_ERROR = "CHECK_SESSION_ERROR";

export const LOGOUT_USER = 'LOGOUT_USER';
export const LOGOUT_USER_OK = 'LOGOUT_USER_OK';
export const LOGOUT_USER_ERROR = 'LOGOUT_USER_ERROR';

var url = ''

    process.env.NODE_ENV === "production"
  ? (url = "")
  : (url = "http://localhost:4000");

export const registerUser = (form) => {
    return async (dispatch) => {
      dispatch({ type: AUTH_REGISTER });
  
      const request = await fetch(url + "/auth/register", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
        },
        credentials: "include",
        body: JSON.stringify(form),
      });
      console.log('hola')
      const result = await request.json();
  
      if (request.ok) {
        dispatch({ type: AUTH_REGISTER_OK, payload: result });
      } else {
        dispatch({ type: AUTH_REGISTER_ERROR, payload: false });
      }
    };
  };
  
  export const loginUser = (form) => {
    return async (dispatch) => {
      dispatch({ type: AUTH_LOGIN });
  
      const request = await fetch(url + "/auth/login", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
        },
        credentials: "include",
        body: JSON.stringify(form),
      });
      
      const result = await request.json();
      
      console.log(result);
      if (request.ok) {
        dispatch({ type: AUTH_LOGIN_OK, payload: result });
      } else {
        console.log(result.message);
        dispatch({ type: AUTH_LOGIN_ERROR, payload: result.message });
      };
    };
  };
  
  export const checkUserSession = () => {
    return async (dispatch) => {
      dispatch({ type: CHECK_SESSION });
      const request = await fetch(url + "/auth/check-session", {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
        },
        credentials: "include",
      });
  
      const result = await request.json();
  
      if (request.ok) {
        dispatch({ type: CHECK_SESSION_OK, payload: result });
      } else {
        dispatch({ type: CHECK_SESSION_ERROR });
      }
    };
  };
  

  export const logoutUser = () => {
      return async (dispatch) => {
        dispatch({type: LOGOUT_USER})
        const request = await fetch(url + '/auth/logout',{
      method: 'POST',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Acces-Control.Allow-Origin': '*',
      },
      credentials: 'include', /* para securizar mas la peticion */
  });
    const result = await request.json()

    if (request.ok) {
       dispatch({ type: LOGOUT_USER_OK, payload: result})
    } else {
        dispatch({ type: LOGOUT_USER_ERROR })
    }  
      }
  }