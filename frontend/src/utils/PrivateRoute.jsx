import { connect } from "react-redux";
import { useLocation, Navigate } from "react-router-dom";

const PrivateRoute = ({user, component, ...restProps}) => {
    const location = useLocation();

    if(!component) throw new Error("You must provide a component to <PrivateRoute component={...} />")

    if (user === null) return <div>Loading user...</div>
    
    if(user === false) return <Navigate to='/login' state={{prevRoute: location.pathname}} />

    if (user) return component
};

export default connect(({auth}) =>({user: auth.user}) )(PrivateRoute)